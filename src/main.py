import pyRandomdotOrg


rnd = pyRandomdotOrg.clientlib("test_roulette","evdoxx@gmail.com")


mapping = {
    1: 'Red',
    2: 'Black',
    3: 'Odd',
    4: 'Even',
    5: '1 to 18',
    6: '19 to 36'
}

print rnd.QuotaChecker()
rand_list =  rnd.IntegerGeneratorList(1000,1,6, rnd='new')

output = ['{}: {}\n'.format(i+1, mapping[case]) for i, case in enumerate(rand_list)]

with open('../files/output.txt', 'w') as f:
    f.writelines(output)